import {CanvasImage} from '../Canvas/CanvasImage';
import {Texture} from './Texture';


const textures: Array<Texture> = [];

function getAllTexture() {

  const textures_path = [
    'block/bricks.png',
    'block/stone_bricks.png',
    'wall_1.png',
    'wall_2.png'
  ];


  const promises: Array<Promise<void>> = [];

  textures_path.forEach((path) => {
    const image = new Image();
    image.src = `assets/${path}`;

    promises.push(new Promise(resolve => {
      image.addEventListener('load', () => {
        const canvas = new CanvasImage(image);
        textures.push(canvas.getTexture());
        canvas.delete();
        resolve();
      });
    }));

  });

  return Promise.all(promises);
}

export {
  textures,
  getAllTexture
};

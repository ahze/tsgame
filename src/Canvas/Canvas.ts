import {Vector2} from '../Vector2';
import {Game} from '../Game';

export abstract class Canvas {

  private static id = 0;
  protected contextd2D: CanvasRenderingContext2D;
  protected canvas: HTMLCanvasElement;
  protected view_angle = 10;

  constructor(public size: Vector2) {
    const canvas = this.createCanvas();
    const context = canvas.getContext('2d') as CanvasRenderingContext2D;
    context.lineCap = 'round';
    context.lineJoin = 'round';
    context.strokeStyle = 'black';
    context.lineWidth = 1;

    this.canvas = canvas;
    this.contextd2D = context;
  }

  abstract drawContext(context: Game): void;

  protected drawLine(x1: number, y1: number, x2: number, y2: number): void {
    this.contextd2D.beginPath();
    this.contextd2D.moveTo(x1, y1);
    this.contextd2D.lineTo(x2, y2);
    this.contextd2D.stroke();
  }

  public drawSquare(x: number, y: number, width: number, height: number): void {
    this.contextd2D.fillRect(x, y, width, height);
  }

  protected reset(): void {
    this.contextd2D.fillStyle = 'white';
    this.drawSquare(0, 0, this.size.x, this.size.y);
  }

  protected setStrokeColor(red: number, green: number, blue: number): void {
    this.contextd2D.strokeStyle = `rgb(${red}, ${green}, ${blue})`;
  }

  public setFillColor(red: number, green: number, blue: number): void {
    this.contextd2D.fillStyle = `rgb(${red}, ${green}, ${blue})`;
  }

  private createCanvas(): HTMLCanvasElement {
    const id = 'canvas_' + Canvas.id++;
    const canvasElement = document.createElement('canvas');
    canvasElement.setAttribute('id', id);
    canvasElement.setAttribute('width', this.size.x + '');
    canvasElement.setAttribute('height', this.size.y + '');
    canvasElement.setAttribute('style', 'border: 3px solid black');
    (document.getElementById('canvas') as HTMLElement).appendChild(canvasElement);
    return canvasElement;
  }

}

import {Canvas} from './Canvas';
import {Game} from '../Game';
import {degreToRadian, two_pi} from '../utils';
import {textures} from '../Texture/load_textures';
import {Pixel} from '../Texture/Texture';
import {environment} from '../main/environments/environment';

export class CanvasRaycast extends Canvas {

  private readonly lineWidth: number;
  private readonly rayPerDegre: number;
  private shadow_ratio = 0.3;

  constructor(height: number) {
    super({x: height * 16 / 9, y: height});
    this.view_angle = 80;
    this.rayPerDegre = 4;
    this.lineWidth = this.size.x / (this.view_angle * this.rayPerDegre);
    this.lineWidth = Math.round(this.lineWidth);
    this.size.x = this.lineWidth * this.rayPerDegre * this.view_angle;
    this.canvas.setAttribute('width', this.size.x.toString());
  }

  drawContext(game: Game): void {
    this.reset();
    if (!environment.draw3d) {
      return;
    }
    const map = game.map;
    const player_pos = game.player.pos;
    const tile_size = 8;

    for (let i = -this.view_angle / 2; i < this.view_angle / 2; i++) { // loop on each degree
      for (let j = 0; j < this.rayPerDegre; j++) { // loop on each ray per degree

        const ray_angle = game.player.angle + (i + j / this.rayPerDegre) * degreToRadian; // get the angle of the actual ray
        const nextWall = map.getNextPoint(player_pos, ray_angle); // calculate where the ray goes
        if (nextWall.distance === Infinity) {
          continue;
        }

        const diff_angle = (game.player.angle - ray_angle + two_pi) % two_pi;
        const dist = nextWall.distance * tile_size * Math.cos(diff_angle); // diff_angle to fix fish eye effect


        const lineHeight = (tile_size * this.size.y) / dist;
        const tile_y_step = 1 / tile_size;
        const tile_y_offset = 0;

        const base_y = this.size.y / 2 - lineHeight / 2; // centering the line
        let base_x = i + (j / this.rayPerDegre) + this.view_angle / 2;
        base_x *= this.lineWidth * this.rayPerDegre;

        /*
        for(let x_line = 0; x_line < this.lineWidth; x_line++) {
          this.drawLine(base_x + x_line, base_y, base_x + x_line, base_y + lineHeight);
        }
         */

        // draw walls
        const col = textures[nextWall.from === 'VERTICAL' ? 0 : 1].columns[Math.floor(nextWall.wallCol * tile_size)] as Pixel[];
        let tile_y = tile_y_offset * tile_y_step;
        const line_ratio = Math.min(lineHeight, this.size.y) / this.size.y;
        for (let y = 0; y < tile_size && base_y + y * lineHeight / tile_size < this.size.y; y++) {
          const p: Pixel = col[Math.floor(tile_y * tile_size)];
          const color_ratio = -255 * this.shadow_ratio * (1 - line_ratio);
          this.setFillColor(p.r + color_ratio, p.g + color_ratio, p.g + color_ratio);
          this.drawSquare(base_x, base_y + y * lineHeight / tile_size, this.lineWidth, lineHeight / tile_size + 1);
          tile_y += tile_y_step;
        }
      }
    }

  }


}
